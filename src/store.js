import { configureStore } from "@reduxjs/toolkit";
import categoryReducer from "./features/categories/categorySlice";
import newProductsReducer from "./features/new/newProductsSlice";
import popularProductsReducer from "./features/popularProducts/popularProductsSlice";
import promotionReducer from "./features/promotion/promotionSlice";

const store = configureStore({
  reducer: {
    categories: categoryReducer,
    newProducts: newProductsReducer,
    popularProducts: popularProductsReducer,
    promotion: promotionReducer,
  },
});
export default store;
