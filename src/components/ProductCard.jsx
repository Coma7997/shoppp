import React from "react";
import { Link } from "react-router-dom";
import basket from "../assets/card/basket.svg";
import like from "../assets/card/like.svg";
const ProductCard = ({ product }) => {
  return (
    <div className="min-w-[230px] h-[300px] xl:w-[300px] xl:h-[410px] flex flex-col justify-between gap-2 bg-white rounded-xl p-3 mb-9 mr-5">
      <div className="flex items-center justify-center w-full h-[170px] xl:h-[200px]">
        <img src={product.image} className="w-full max-h-full" />
      </div>
      <h1 className="line-clamp-2 xl:line text-sm xl:text-md xl:h-[40px]">
        {product.name}
      </h1>

      <p className="font-bold xl:text-[19px]">{product.price} сум</p>
      <div className="flex justify-between">
        <Link to="/card">
          <button className="btn flex items-center gap-3 xl:px-5">
            <img src={basket} alt="" />
            <span>В корзину</span>
          </button>
        </Link>
        <Link to="/like">
          <button className="btn">
            <img src={like} alt="" />
          </button>
        </Link>
      </div>
    </div>
  );
};

export default ProductCard;
