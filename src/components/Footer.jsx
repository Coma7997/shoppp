import React from "react";
import logof from "../assets/footer/logof.svg";
import call from "../assets/footer/call.svg";
import insta from "../assets/footer/insta.svg";
import telega from "../assets/footer/telega.svg";
import facebook from "../assets/footer/facebook.svg";
import loc from "../assets/footer/loc.svg";

const Footer = () => {
  return (
    <footer className="relative lg:flex justify-between gap-32 p-4 pb-10 lg:p-8 text-white">
      <div className="mb-9">
        <div className="flex items-center gap-2 cursor-pointer mb-3">
          <img src={logof} alt="Navbar logo" />
          <div className="text-center">
            <h1 className="text-lg lg:text-2xl font-semibold tracking-wider">
              TEPLODOM
            </h1>
            <p className="text-[10px] lg:text-sm leading-4">
              Интернет магазин <br /> строй материалов
            </p>
          </div>
        </div>

        <div className="hidden md:flex items-start gap-1">
          <img src={loc} alt="" />
          <span className="">
            ул.Уста Ширин, рынок <br /> Джамий, дом 134
          </span>
        </div>
      </div>

      <div className="flex flex-col sm:items-center sm:justify-between sm:flex-1 lg:flex-none xl:flex-1 flex-wrap sm:flex-row gap-5 lg:gap-10 text-lg mb-9">
        <div className="flex flex-col gap-6 md:gap-1">
          <a className="font-bold md:mb-4">Быстрые ссылки</a>
          <a>Мастерам</a>
          <a>Оформление заказа</a>
          <a>Пользовательское соглашение</a>
        </div>
        <div className="flex flex-col gap-6 md:gap-1">
          <a className="font-bold md:mb-4">Полезное</a>
          <a>О нас</a>
          <a>Поставщикам</a>
          <a>Возврат товара</a>
        </div>
        <div className="flex flex-col gap-6 md:gap-1">
          <a className="font-bold md:mb-4">Контакты</a>
          <span className="flex items-center gap-2">
            <img src={call} /> +998 (99) 822-22-21
          </span>
          <span className="flex items-center gap-2">
            <img src={call} /> +998 (33) 200-72-77
          </span>
          <span className="flex gap-7 mt-[10px]">
            <a href="https://t.me/ismoilzabihullayev">
              <img src={telega} className="cursor-pointer" />
            </a>
            <a href="https://www.instagram.com/ismoilzabihullayev">
              <img src={insta} className="cursor-pointer" />
            </a>
            <a href="https://www.facebook.com/samuel-2007/">
              <img src={facebook} className="cursor-pointer" />
            </a>
          
          </span>
        </div>
      </div>

      <div className="flex items-start gap-1 md:hidden">
        <img src={loc} alt="" />
        <span className="">
          ул.Уста Ширин, рынок <br /> Джамий, дом 134
        </span>
      </div>
      <div className="absolute bottom-0 left-2/4 -translate-x-2/4 flex py-4 lg:pb-4">
        © 2024 Teplodom. Все права защищены
      </div>
    </footer>
  );
};

export default Footer;
