import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getNewProducts } from "./newProductsSlice";
import ProductCard from "../../components/ProductCard";
import { Link } from "react-router-dom";

const AllNewProducts = () => {
  const { newProducts } = useSelector((state) => state.newProducts);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getNewProducts());
  }, []);
  return (
    <div className="container my-[76px]">
      <div className="flex items-center justify-between mb-8">
        <h1 className="text-xl lg:text-3xl">Все новинки на сайте</h1>
      </div>
      <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto gap-4 py-5">
        {newProducts?.map((product, i) => {
          if (i >= 5) {
            return null;
          } else {
            return <ProductCard key={product.id} product={product} />;
          }
        })}
      </div>
      <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto gap-4 py-5">
        {newProducts?.map((product, i) => {
          if (i < 5) {
            return null;
          } else {
            return <ProductCard key={product.id} product={product} />;
          }
        })}
      </div>
    </div>
  );
};

export default AllNewProducts;
