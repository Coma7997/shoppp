import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getCategories } from "./categorySlice";

const NewCotegories = () => {
  const { categories } = useSelector((state) => state.categories);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategories());
  }, []);
  return (
    <div className="container my-[76px]">
      <div className="flex items-center justify-between mb-8">
        <h1 className="text-xl lg:text-3xl mt-7">Новый продукты</h1>
      </div>
      <div className="flex overflow-x-scroll xl:justify-between xl:overflow-x-auto xl:flex-wrap">
        {categories?.map((category) => {
          return (
            <div
              key={category.id}
              className="min-w-[140px] h-[170px] lg:w-[170px] lg:h-[200px] flex flex-col bg-white rounded-xl p-3 cursor-pointer gap-9"
            >
              <div className="flex flex-1 items-center justify-center w-full h-full">
                <img src={category.image} className="max-w-[110px]" />
              </div>

              <p className="text-center line-clamp-2">{category.name}</p>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default NewCotegories;
