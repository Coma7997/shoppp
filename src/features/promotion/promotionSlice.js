import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  promotion: [],
};

const promotionSlice = createSlice({
  name: "promotion",
  initialState,
  reducers: {
    getPromotion: (state, { payload }) => {
      state.promotion = payload;
    },
  },
});

export default promotionSlice.reducer;
export function getPromotion() {
  return async function (dispatch) {
    const res = await fetch(`https://shoppp.vercel.app:3333/promotion`);
    const data = await res.json();
    dispatch({ type: "promotion/getPromotion", payload: data });
  };
}
