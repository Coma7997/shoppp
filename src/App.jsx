import React from "react";
import { RouterProvider, createBrowserRouter } from "react-router-dom";
import RootLayout from "./layout/RootLayout";
import Home from "./components/Home";
import AllNewProducts from "./features/new/AllNewProducts";
import WatchNewProducts from "./features/new/WatchNewProducts";
import WatchNewCotegories from "./features/categories/WatchNewCotegories";

import AllCategories from "./features/categories/AllCategories";
import AllPopularProducts from "./features/popularProducts/AllPopularProducts";
import Curer from "./features/Curecr/Curer";
import NewCotegories from "./features/categories/NewCotegories";
import Phone from "./features/Curecr/Phone";
import Card from "./features/Card/Card";
import Like from "./features/Like/Like";

const App = () => {
  const router = createBrowserRouter([
    {
      path: "/",
      element: <RootLayout />,
      children: [
        {
          index: true,
          element: <Home />,
        },
        {
          path: "new",
          element: <AllNewProducts />,
        },
        {
          path: "watch/new/products",
          element: <WatchNewProducts />,
        },
        {
          path: "watch/new/categories",
          element: <WatchNewCotegories />,
        },
        {
          path: "categories",
          element: <AllCategories />,
        },
        {
          path: "new/cotegories",
          element: <NewCotegories />,
        },

        {
          path: "popular",
          element: <AllPopularProducts />,
        },
        {
          path: "curer",
          element: <Curer />,
        },
        {
          path: "phone",
          element: <Phone />,
        },
        {
          path: "card",
          element: <Card />,
        },
        {
          path: "like",
          element: <Like />,
        },
      ],
    },
  ]);
  return (
    <>
      <RouterProvider router={router} />
    </>
  );
};

export default App;
